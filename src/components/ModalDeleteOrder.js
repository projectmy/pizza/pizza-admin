import { Container, Grid, Alert, Button, Modal, Snackbar, Typography, Box } from "@mui/material"
import { useEffect, useState } from "react";

function ModalDeleteUser ({openModalDelete, idDelete, handleCloseDelete, style, setVarRefeshPage, varRefeshPage}) {
    const [openAlert, setOpenAlert] = useState(false)
    const [statusModalDelete, setStatusModalEdit] = useState("error")
    const [noidungAlert, setNoidungAlert] = useState("")

    const handleCloseAlert = () => {
        setOpenAlert(false)
    }

    const onBtnCancelClick = () => {
        handleCloseDelete()
    }
    const onBtnConfirmDeleteClick = () => {
        console.log("Xác nhận xóa được click!")
        const vURL_DELETE = "http://42.115.221.44:8080/devcamp-pizza365/orders/" + idDelete
        fetch(vURL_DELETE, { method: 'DELETE' })
            .then(async response => {
                const isJson = response.headers.get('content-type')?.includes('application/json');
                const data = isJson && await response.json();
                // check for error response
                if (!response.ok) {
                    // get error message from body or default to response status
                    const error = (data && data.message) || response.status;
                    return Promise.reject(error);
                }
                console.log('Delete successful');
                setOpenAlert(true)
                setStatusModalEdit("success")
                setNoidungAlert("Xóa Order " + {idDelete} + " thành công!")
                setVarRefeshPage(varRefeshPage + 1)
                handleCloseDelete()
            })
            .catch(error => {
                console.error('There was an error!', error);
                setOpenAlert(true)
                setStatusModalEdit("error")
                setNoidungAlert("Xóa Order " + {idDelete} + " thất bại!")
                handleCloseDelete()
            });
    }
    return (
        <>
            <Modal
            open={openModalDelete}
            onClose={handleCloseDelete}
            aria-labelledby="modal-delete"
            aria-describedby="modal-delete-user"
            >
            <Box sx={style} style={{backgroundColor:"#f8bbd0"}}>
                <Typography align="center" mb={2} id="modal-modal-title" variant="h4">
                    <strong style={{color:'#ad1457'}}>Delete Order?</strong>
                </Typography>
                    <Grid container mt={5}>
                        <Grid item sm={12}>
                            <Typography variant="h6">Bạn có thật sự muốn xóa Order có ID: <b>{idDelete}</b> chứ?</Typography>
                        </Grid>
                    </Grid>
                    <Grid container className="mt-4 text-center">
                        <Grid item sm={12}>
                            <Grid container mt={4}>
                                <Grid item sm={6}>
                                    <Button onClick={onBtnConfirmDeleteClick} className="bg-danger w-100 text-white">Xác nhận</Button>
                                </Grid>
                                <Grid item sm={6}>
                                    <Button onClick={onBtnCancelClick} className="bg-secondary w-75 text-white">Hủy Bỏ</Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
            </Box>
        </Modal>
        <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
            <Alert onClose={handleCloseAlert} severity={statusModalDelete} sx={{ width: '100%' }}>
            {noidungAlert}
            </Alert>
      </Snackbar>
        </>
    )
}

export default ModalDeleteUser